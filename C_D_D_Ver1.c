//AUTHORS: CJ DVORAK., REAVE HOSMAN.,RON GILLMAN
//NAME: C&D&D
//DUE: DEC 11TH
//DESCRIPTION: A FULL D&D CHARACTER CREATOR FOR EASIER STAT TRACKING WITH SOME COOL ADDITIONS LIKE EDIT CHARACTER AND OS FUNCTIONS.

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

///////////////
/// DEFINES ///
///////////////
#define STRENGTH 0
#define DEXTERITY 1
#define CONSTITUTION 2
#define INTELLIGENCE 3
#define WISDOM 4
#define CHARISMA 5
#define ACROBATICS 6
#define ANIMAL_HANDLING 8
#define ARCANA 10
#define ATHLETICS 12
#define DECEPTION 14
#define HISTORY 16
#define INSIGHT 18
#define INTIMIDATION 20
#define INVESTIGATION 22
#define MEDICINE 24
#define NATURE 26
#define PERCEPTION 28
#define PERFORMANCE 30
#define PERSUASION 32
#define RELIGION 34
#define SLEIGHT_OF_HAND 36
#define STEALTH 38
#define SURVIVAL 40

//////////////////////////
/// RACES ASK FOR ASI? ///
//////////////////////////
#define HILL_DWARF 0
#define MOUNTAIN_DWARF 1
#define WOOD_ELF 2
#define HIGH_ELF 3
#define DROW 4
#define FOREST_GNOME 5
#define ROCK_GNOME 6
#define STOUT_HALFLING 7
#define LIGHTFOOT_HALFLING 8
#define DRAGONBORN 9
#define HALF_ELF 10
#define HALF_ORC 11
#define TIEFLING 12
#define HUMAN 13


///////////////
/// CLASSES ///
///////////////
#define BARBARIAN 0
#define BARD 1
#define CLERIC 2
#define DRUID 3
#define FIGHTER 4
#define MONK 5
#define PALADIN 6
#define RANGER 7
#define ROGUE 8
#define SORCERER 9
#define WARLOCK 10
#define WIZARD 11

/////////////////
//// STRUCTS ///
////////////////

struct class //GENERIC CLASS
{
	int hitDice;
	char savingProf[20];
	int pageNO;
};

/* Basic info for Basic Info Array
int class; [0]
int level [1]
int currentXP[2]
int race;   [3]
int currentXP; [4]
int maxHp [5]
int currHP [6]
int currHitDice [7]
*/

/* Stat Array
 *[0] STRENGTH
 *[1] DEX
 *[2] CONSTITUTION
 *[3] INTELLIGENCE
 *[4] WISDOM
 *[5] CHARISMA
 */
struct playerCharacter
{
	char characterName[20];
	int scores[6]; //Saving Throws
	int currency[5]; //Different types of coin
	int basicInfoArray[8]; //SEE ABOVE FOR INFO
	int statArray[6]; //SEE ABOVE FOR INFO
	unsigned long long proficiencyBits;
	int tempHP; //Temporary HP
	struct class classInfo; //Extra class info for Player Character.
};

//////////////////
/// Prototypes ///
//////////////////
//THE BASICS
void basicMenu(); //Prints out menu commands
void selectionInterchange(struct playerCharacter *playerArray,int playerAmount,char userAnswer); //Goes into selected method from menu option user selected

//STAT ASSIGNMENT
int assignStatValue(int *diceSum); //Allows players to assign stat points to their character attributes
void rollCharacterStats(int statArray[]); //Goes through each stat and lets player assign points.

//EDIT PROTOS
void editMenu(); //prints out edit menu commands
void editInterchange(int charChoice, struct playerCharacter *playerArray, int editChoice);//Goes into selected method from menu option user selected
void editStats(int statArray[]); //Allows player to edit their stats.
void editCharacters(struct playerCharacter *playerArray, int playerAmount); //allows player to choose character to edit and select an edit command.

//HELPER PROTOTYPES
void printStatArray(int statArray[]); //Prints out characters stats
int diceRollAdder(); //Rolls dice and adds them together removing lowest value.
int dice(int sides);
void getClass(int class); //Parses which class their character or other characters have
void getRace(int race); //Parses which race their or other characters have.
void printBasicStats(int basicStats[]); //Print out characters basic stats
void printPlayerInfo(struct playerCharacter *currentPlayer, int playerAmount); //Print out selected characters info

//SAVE AND LOAD PROTOS
void load(struct playerCharacter *currentPlayer, int playerAmount); //Loads characters info from a text file.
void save(struct playerCharacter *currentPlayer, int playerAmount); //Saves all characters info into a text file.

//SYSTEM PROTOS
void editInfo(); //OS command to open a text editor to edit the load file
void sourceInfo(); //Opens a web browser to the git repo. (LINUX ONLY AT THE MOMENT)

//CLASS STRUCT PROTOS
void chooseProficiencies(struct playerCharacter *p);
void chooseProficienciesPrintOptions(unsigned long long proficiencyChoices);
int getProficiency(unsigned long long proficiencyBits, int throwOrSkill);
void setProficiency(unsigned long long *proficiencyBits, int throwOrSkill, unsigned long long proficiency);
int hitDiceType(int class);
int pageNumber(int class);

//RACE PROTOS
void addASIs(struct playerCharacter *p);

//DM PROTOS
void getEncounter();

//CHAR CREATION PROTOS
void startingGP(struct playerCharacter *p);
struct playerCharacter createPlayerCharacter();

int main()
{
    struct playerCharacter *playerArray; //Dynamic array of characters for DnD.
    int playerAmount; //Amount of players/characters playing
    char userAnswer[2]; //Answers what they want to do from main menu.
    userAnswer['/0'];
    int done = 0; //Do they want to quit?

    printf("HOW MANY PLAYERS ARE PLAYING OR BEING LOADED?\n");
    scanf("%d",&playerAmount);

    playerArray = (struct playerCharacter*) malloc(playerAmount * sizeof(struct playerCharacter)); //Dynamic array of characters.
    
    printf("*----------------------------------------------*\n");
    printf("|ISSUES:                                       |\n");
    printf("|----------------------------------------------|\n");
    printf("|* Save and Load can only handle one character.|\n");
    printf("|* View Source Code is Linux Only.             |\n");
    printf("|* Edit Load File may not work on Windows      |\n");
    printf("*----------------------------------------------*\n");

    while(!done)
    {
        basicMenu();
        scanf("%s",userAnswer);

        selectionInterchange(playerArray,playerAmount,userAnswer[0]);

        if(userAnswer[0] == 'q')
        {
            done = 1;
        }
    }

    free(playerArray);
    system("pause"); // for Windows...
    return 0;
}

/*
//Author: Ron Gillman
//FunctionName: selectionInterchange
//Description: Receives userAnswer and goes to function pertaining to that userAnswer
//Parameters: playerArray: Dynamic array of playerCharacters, playerAmount: how many characters are in game, userAnswer: user answer from basicMenu
//Return: NONE
*/
void selectionInterchange(struct playerCharacter *playerArray,int playerAmount,char userAnswer)
{
    if(userAnswer == 'i') //Import Characters
    {
        load(playerArray, playerAmount);
    }
    else if(userAnswer == 'n') //Character Creation
    {
        for (int i = 0; i < playerAmount; i++)
        {
            playerArray[i] = createPlayerCharacter();
        }
    }
    else if(userAnswer == 'g') //Generate Encounter
    {
        getEncounter();
    }
    else if(userAnswer == 'e') //Edit Character Mode
    {
        editCharacters(playerArray, playerAmount);
    }
    else if(userAnswer == 'l') //Edit Load Files
    {
        editInfo();
    }
    else if(userAnswer == 's') //Save current characters
    {
        save(playerArray, playerAmount);
    }
    else if(userAnswer == 'v') //View Source Info
    {
        sourceInfo();
    }
    else if(userAnswer == 'p')
    {
        printPlayerInfo(playerArray, playerAmount);
    }
    else
    {
        printf("This is not a valid choice. Going back to Main Menu.\n");
    }
}

/*
//Author: Ron Gillman
//FunctionName: basicMenu
//Description: Shows Selection menu for user, returns their selection.
//Parameters: NONE
//Return: int
*/
void basicMenu()
{
	printf("*-------------------------*\n");
	printf("|     C&D&D Version 1     |\n");
	printf("*-------------------------*\n");
	printf("|  Import Characters [i]  |\n");
	printf("| Make New Characters [n] |\n");
	printf("| View Character Info [p] |\n");
	printf("|  Generate Encounter[g]  |\n");
	printf("| Edit Character Mode [e] |\n");
	printf("|    Edit Load File [l]   |\n");
    printf("|    Save Characters [s]  |\n");
	printf("|   View Source Code [v]  |\n");
	printf("|     View Credits [c]    |\n");
	printf("|         Quit [q]        |\n");
	printf("*-------------------------*\n");

	printf("Make a selection:\n");
}

 //////////////////////////////
 // Assigning Stat Functions //
 //////////////////////////////
/*
//Author: Ron Gillman
//FunctionName: rollCharacterStats
//Description: Lets players choose their stat value after getting the sum of 3 d6 dice rolls
//Parameters: *player: pointer to current player.
//Return: N/A
 */
void rollCharacterStats(int statArray[])
{
    int diceSum = diceRollAdder(); //sum of 3 highest, out of 4, dice rolls

    for (int k = 0; k < 6 && diceSum != 0; k++)
    {
        printf("You Have %d Points Left! Assign Points for ",diceSum);

        if (k == STRENGTH)
        {
            printf("Strength!\n");
            statArray[STRENGTH] = assignStatValue(&diceSum);
        }
        if (k == DEXTERITY)
        {
            printf("Dexterity!\n");
            statArray[DEXTERITY] = assignStatValue(&diceSum);
        }
        if (k == CONSTITUTION)
        {
            printf("Constitution!\n");
            statArray[CONSTITUTION] = assignStatValue(&diceSum);
        }
        if (k == INTELLIGENCE)
        {
            printf("Intelligence!\n");
            statArray[INTELLIGENCE] = assignStatValue(&diceSum);
        }
        if (k == WISDOM)
        {
            printf("Wisdom!\n");
            statArray[WISDOM] = assignStatValue(&diceSum);
        }
        if (k == CHARISMA)
        {
            printf("Charisma!\n");
            statArray[CHARISMA] = assignStatValue(&diceSum);
        }
    }
}

/*
//Author: Ron Gillman
//FunctionName: assignStatValue
//Description: Lets player choose their stat value
//Parameters: Pass By Reference diceSum: Sum of 3 dice rolls.
//Return: int
 */
int assignStatValue(int *diceSum)
{
    int statValue = 0;
    do
    {
        scanf("%d", &statValue);
        if(statValue > *diceSum)
        {
            printf("Sorry you do not have enough points to assign that value!\n Current points:%d\n",*diceSum);
        }
    }while(statValue > *diceSum);

    *diceSum -= statValue;
    return statValue;
}

//////////////////////////////
// EDIT CHARACTER FUNCTIONS //
//////////////////////////////

/*
//Author: Ron Gillman
//FunctionName: editCharacters
//Description: Allows players to choose a character to edit name, stats, etc.
//Parameters: playerArray: dynamic array of characters playerAmount
//Return: N/A
 */
void editCharacters(struct playerCharacter *playerArray, int playerAmount)
{
    int charChoice; //What character user wants to edit
    int editChoice = 100; // what part of the character they want to edit.

    struct playerCharacter *ptr; //pointer for playerArray
    ptr = playerArray;

    printf("Which character do you want to edit?\n");
    for(int i = 0; i < playerAmount; i++)
    {
        printf("[%d] %s\n",i,ptr->characterName);
        ptr++;
    }
    scanf("%d",&charChoice);

    while(editChoice != 6)
    {
        editMenu();
        scanf("%d", &editChoice);
        editInterchange(charChoice, playerArray, editChoice);
    }
}

/*
//Author: Ron Gillman
//FunctionName: editMenu
//Description: Displays menu of possible character Edits.
//Parameters: N/A
//Return: N/A
 */
void editMenu()
{

    printf("*------------------*\n");
    printf("|  Character Edit  |\n");
    printf("*------------------*\n");
    printf("|     Name [0]     |\n");
    printf("|    Stats [1]     |\n");
    printf("|  Current EXP [2] |\n");
    printf("|  Current HP [3]  |\n");
    printf("|    Max HP [4]    |\n");
    printf("|   Temp HP [5]    |\n");
    printf("|      Quit[6]     |\n");
    printf("*------------------*\n");

    printf("What do you want to edit?\n");
}

/*
//Author: Ron Gillman
//FunctionName: editInterchange
//Description: From user selection changes stats of the selection.
//Parameters: charChoice: Character that has been chosen, playerArray: dynamic array of DnD characters, editChoice: user chosen int value for what they want to edit.
//Return: N/A
 */
void editInterchange(int charChoice, struct playerCharacter *playerArray, int editChoice)
{
    char newName[20]; //name to replace original characters name
    int newValue; // value to replace original stat

    if(editChoice == '0') //Character Name
    {
        printf("Enter a new name for Character:\n");
        scanf("%s",newName);

        strcpy(playerArray[charChoice].characterName,newName);
    }
    else if(editChoice == '1') //Stats
    {
        editStats(playerArray[charChoice].statArray);
    }
    else if(editChoice == '3') //CurrentHP
    {
        printf("Current HP: %d", playerArray[charChoice].basicInfoArray[6]);
        printf("What do you want to change it to?\n");
        scanf("%d",&newValue);
        playerArray[charChoice].basicInfoArray[6] = newValue;
    }
    else if(editChoice == '5') //TempHP
    {
        int newTempHP;
        printf("Temp Value: %d",playerArray[charChoice].tempHP);
        printf("Enter a new temp HP:\n");
        scanf("%d",&newTempHP);
        playerArray[charChoice].tempHP = newTempHP;
    }
    else if(editChoice == '2') //Current EXP
    {
        printf("Current Exp: %d\n What do you want to change it to?\n",playerArray[charChoice].basicInfoArray[2]);
        scanf("%d",&newValue);
        playerArray[charChoice].basicInfoArray[2] = newValue;

    }
    else if(editChoice == '4')
    {
        printf("Maximum HP: %d", playerArray[charChoice].basicInfoArray[5]);
        printf("What do you want to change it to?\n");
        scanf("%d",&newValue);
        playerArray[charChoice].basicInfoArray[5] = newValue;
    }
    else
    {
        printf("That is not a valid choice, going back to Edit Menu.\n");
    }
}

/*
//Author: Ron Gillman
//FunctionName: editInterchange
//Description: Goes through character Stat array and allows user to change their values.
//Parameters: statArray: array of standard stats for a character
//Return: N/A
 */
void editStats(int statArray[])
{
    int newStatValue; //value that replaces original stat value.

    for (int k = 0; k < 6; k++)
    {
        if (k == STRENGTH)
        {
            printf("Current Strength: %d\n",statArray[STRENGTH]);
            printf("What do you want to change it to?\n");
            scanf("%d", &newStatValue);
            statArray[STRENGTH] = newStatValue;
        }
        if (k == DEXTERITY)
        {
            printf("Current Strength: %d\n",statArray[DEXTERITY]);
            printf("What do you want to change it to?\n");
            scanf("%d", &newStatValue);
            statArray[DEXTERITY] = newStatValue;
        }
        if (k == CONSTITUTION)
        {
            printf("Current Strength: %d\n",statArray[CONSTITUTION]);
            printf("What do you want to change it to?\n");
            scanf("%d", &newStatValue);
            statArray[CONSTITUTION] = newStatValue;
        }
        if (k == INTELLIGENCE)
        {
            printf("Current Strength: %d\n",statArray[INTELLIGENCE]);
            printf("What do you want to change it to?\n");
            scanf("%d", &newStatValue);
            statArray[INTELLIGENCE] = newStatValue;
        }
        if (k == WISDOM)
        {
            printf("Current Strength: %d\n",statArray[WISDOM]);
            printf("What do you want to change it to?\n");
            scanf("%d", &newStatValue);
            statArray[WISDOM] = newStatValue;
        }
        if (k == CHARISMA)
        {
            printf("Current Strength: %d\n",statArray[CHARISMA]);
            printf("What do you want to change it to?\n");
            scanf("%d", &newStatValue);
            statArray[CHARISMA] = newStatValue;
        }
    }
}

/////////////////////////////
// SAVE AND LOAD FUNCTIONS //
/////////////////////////////

/*
//Author: Ron Gillman
//FunctionName: save
//Description: Saves all characters info into a txt file.
//Parameters: *currentPlater: pointer to current player. playerAmount: amount of players that are playing
//Return: N/A
*/
void save(struct playerCharacter *currentPlayer, int playerAmount)
{
    FILE *fp; //File Pointer

    char saveLocation[50]; //file path to where they want to save their file.
    printf("Enter your desired save location!");
    scanf("%s",saveLocation);

    fp = fopen(saveLocation, "w+");
    int tempData; //data to be written to file
    unsigned long long int tempProf; //proficiencies to be written to file.

    for(int i = 0; i < playerAmount; i++)
    {
        fputs(currentPlayer[i].characterName, fp);
        for (int j = 0; j < 8; j++)
        {
            tempData = currentPlayer[i].basicInfoArray[j];
            _putw(tempData, fp);
        }
        for (int j = 0; j < 5; j++)
        {
            tempData = currentPlayer[i].currency[j];
            _putw(tempData, fp);
        }
        tempProf = currentPlayer[i].proficiencyBits;
        _putw(tempProf, fp);
        for (int j = 0; j < 6; j++)
        {
            tempData = currentPlayer[i].scores[j];
            _putw(tempData, fp);
        }
        for (int j = 0; j < 6; j++)
        {
            tempData = currentPlayer[i].statArray[j];
            _putw(tempData, fp);
        }
        tempData = currentPlayer[i].tempHP;
        _putw(tempData, fp);
    }

    printf("SAVE FILE CREATED LOCATION:");
    printf(saveLocation);
    //printf(WINDOWS); //FIGURE OUT A WAY TO DEAL WITH THIS.. MAYBE A WHERE DO YOU WANT TO SAVE?
    //printf(CUSTOM);
    printf("\n");

    fclose(fp);
}

/*
//Author: Ron Gillman
//FunctionName: load
//Description: Loads all characters back into game after being given a load location
//Parameters: *currentPlayer: pointer to current player. playerAmount: amount of players that are playing
//Return: N/A
*/
void load(struct playerCharacter *currentPlayer,int playerAmount)
{
    char tempName[20];
    int tempData;
    unsigned long long int tempProf;

    char loadLocation[50];
    printf("Enter your desired load location!");
    scanf("%s",loadLocation);

    FILE *fp;
    fp = fopen(loadLocation, "r");

    for(int i = 0; i < playerAmount; i++)
    {
        fgets(tempName, 20, fp);
        strcpy(currentPlayer[i].characterName,tempName);

        for (int j = 0; j < 8; j++)
        {

            tempData = _getw(fp);
            currentPlayer[i].basicInfoArray[j] = tempData;

        }
        for (int j = 0; j < 5; j++)
        {
            tempData = _getw(fp);
            currentPlayer[i].currency[j] = tempData;

        }
        tempProf = _getw(fp);
        currentPlayer[i].proficiencyBits = tempProf;

        for (int j = 0; j < 6; j++)
        {
            tempData = _getw(fp);
            currentPlayer[i].scores[j] = tempData;
        }
        for (int j = 0; j < 6; j++)
        {
            tempData = _getw(fp);
            currentPlayer[i].statArray[j] = tempData;
        }
        tempData = _getw(fp);
        currentPlayer[i].tempHP = tempData;
    }
}

////////////////////////////
// CLASS STRUCT FUNCTIONS //
////////////////////////////

/*
//Author: CJ Dvorak
//FunctionName: chooseProficiencies
//Description: Asks the user for their chose of skill proficiencies, based on class, and applies them to the player passed in
//Parameters: struct playerCharacter p: pointer to the player to apply choices to
//Return: NONE
*/
void chooseProficiencies(struct playerCharacter *p) {
    unsigned long long profieciencyChoices = 0;
    int numChoices = 0;
    int in = 0;
    switch (p->basicInfoArray[0]) {
        case BARBARIAN:
            numChoices=2;
            setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, NATURE, 1);
            setProficiency(&profieciencyChoices, PERCEPTION, 1);
            setProficiency(&profieciencyChoices, SURVIVAL, 1);
            break;
        case BARD:
            numChoices = 3;
            setProficiency(&profieciencyChoices, ACROBATICS, 1);
            setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
            setProficiency(&profieciencyChoices, ARCANA, 1);
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, DECEPTION, 1);
            setProficiency(&profieciencyChoices, HISTORY, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, INVESTIGATION, 1);
            setProficiency(&profieciencyChoices, MEDICINE, 1);
            setProficiency(&profieciencyChoices, NATURE, 1);
            setProficiency(&profieciencyChoices, PERCEPTION, 1);
            setProficiency(&profieciencyChoices, PERFORMANCE, 1);
            setProficiency(&profieciencyChoices, PERSUASION, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            setProficiency(&profieciencyChoices, SLEIGHT_OF_HAND, 1);
            setProficiency(&profieciencyChoices, STEALTH, 1);
            setProficiency(&profieciencyChoices, SURVIVAL, 1);
            break;
        case CLERIC:
            numChoices = 2;
            setProficiency(&profieciencyChoices, HISTORY, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, MEDICINE, 1);
            setProficiency(&profieciencyChoices, PERSUASION, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            break;
        case DRUID:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
            setProficiency(&profieciencyChoices, ARCANA, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, MEDICINE, 1);
            setProficiency(&profieciencyChoices, NATURE, 1);
            setProficiency(&profieciencyChoices, PERCEPTION, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            setProficiency(&profieciencyChoices, SURVIVAL, 1);
            break;
        case FIGHTER:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ACROBATICS, 1);
            setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, HISTORY, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, PERCEPTION, 1);
            setProficiency(&profieciencyChoices, SURVIVAL, 1);
            break;
        case MONK:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ACROBATICS, 1);
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, HISTORY, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            setProficiency(&profieciencyChoices, STEALTH, 1);
            break;
        case PALADIN:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, MEDICINE, 1);
            setProficiency(&profieciencyChoices, PERSUASION, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            break;
        case RANGER:
            numChoices = 3;
            setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INVESTIGATION, 1);
            setProficiency(&profieciencyChoices, NATURE, 1);
            setProficiency(&profieciencyChoices, PERCEPTION, 1);
            setProficiency(&profieciencyChoices, STEALTH, 1);
            setProficiency(&profieciencyChoices, SURVIVAL, 1);
            break;
        case ROGUE:
            numChoices = 4;
            setProficiency(&profieciencyChoices, ACROBATICS, 1);
            setProficiency(&profieciencyChoices, ATHLETICS, 1);
            setProficiency(&profieciencyChoices, DECEPTION, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, INVESTIGATION, 1);
            setProficiency(&profieciencyChoices, PERCEPTION, 1);
            setProficiency(&profieciencyChoices, PERFORMANCE, 1);
            setProficiency(&profieciencyChoices, PERSUASION, 1);
            setProficiency(&profieciencyChoices, SLEIGHT_OF_HAND, 1);
            setProficiency(&profieciencyChoices, STEALTH, 1);
            break;
        case SORCERER:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ARCANA, 1);
            setProficiency(&profieciencyChoices, DECEPTION, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, PERSUASION, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            break;
        case WARLOCK:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ARCANA, 1);
            setProficiency(&profieciencyChoices, DECEPTION, 1);
            setProficiency(&profieciencyChoices, HISTORY, 1);
            setProficiency(&profieciencyChoices, INTIMIDATION, 1);
            setProficiency(&profieciencyChoices, INVESTIGATION, 1);
            setProficiency(&profieciencyChoices, NATURE, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
            break;
        case WIZARD:
        default:
            numChoices = 2;
            setProficiency(&profieciencyChoices, ARCANA, 1);
            setProficiency(&profieciencyChoices, HISTORY, 1);
            setProficiency(&profieciencyChoices, INSIGHT, 1);
            setProficiency(&profieciencyChoices, INVESTIGATION, 1);
            setProficiency(&profieciencyChoices, MEDICINE, 1);
            setProficiency(&profieciencyChoices, RELIGION, 1);
    }

    printf("You get your choice of %d proficiencies from the following list: \n", numChoices);
    chooseProficienciesPrintOptions(profieciencyChoices);
    while (numChoices > 0) {
        printf("Enter the number of your choice> ");
        scanf("%d", &in);
        if (getProficiency(profieciencyChoices, in) == 1) {
            setProficiency(&(p->proficiencyBits), in, 1);
            setProficiency(&profieciencyChoices, in, 0);
            numChoices--;
        }
        else
            printf("That's not a valid choice\n");
        if (numChoices>0)
            chooseProficienciesPrintOptions(profieciencyChoices);
    }
}

/*
//Author: CJ Dvorak
//FunctionName: chooseProficienciesPrintOptions
//Description: Prints a list of available proficiencies, used in chooseProficiencies
//Parameters: unsigned long long proficiencyChoices: what options are available
//Return: NONE
*/
void chooseProficienciesPrintOptions(unsigned long long proficiencyChoices) {
    if (getProficiency(proficiencyChoices, ACROBATICS)) {
        printf("%d: ACROBATICS\n", ACROBATICS);
    }
    if (getProficiency(proficiencyChoices, ANIMAL_HANDLING)) {
        printf("%d: ANIMAL HANDLING\n", ANIMAL_HANDLING);
    }
    if (getProficiency(proficiencyChoices, ARCANA)) {
        printf("%d: ARCANA\n", ARCANA);
    }
    if (getProficiency(proficiencyChoices, ATHLETICS)) {
        printf("%d: ATHLETICS\n", ATHLETICS);
    }
    if (getProficiency(proficiencyChoices, DECEPTION)) {
        printf("%d: DECEPTION\n", DECEPTION);
    }
    if (getProficiency(proficiencyChoices, HISTORY)) {
        printf("%d: HISTORY\n", HISTORY);
    }
    if (getProficiency(proficiencyChoices, INSIGHT)) {
        printf("%d: INSIGHT\n", INSIGHT);
    }
    if (getProficiency(proficiencyChoices, INTIMIDATION)) {
        printf("%d: INTIMIDATION\n", INTIMIDATION);
    }
    if (getProficiency(proficiencyChoices, INVESTIGATION)) {
        printf("%d: INVESTIGATION\n", INVESTIGATION);
    }
    if (getProficiency(proficiencyChoices, MEDICINE)) {
        printf("%d: MEDICINE\n", MEDICINE);
    }
    if (getProficiency(proficiencyChoices, NATURE)) {
        printf("%d: NATURE\n", NATURE);
    }
    if (getProficiency(proficiencyChoices, PERCEPTION)) {
        printf("%d: PERCEPTION\n", PERCEPTION);
    }
    if (getProficiency(proficiencyChoices, PERFORMANCE)) {
        printf("%d: PERFORMANCE\n", PERFORMANCE);
    }
    if (getProficiency(proficiencyChoices, PERSUASION)) {
        printf("%d: PERSUASION\n", PERSUASION);
    }
    if (getProficiency(proficiencyChoices, RELIGION)) {
        printf("%d: RELIGION\n", RELIGION);
    }
    if (getProficiency(proficiencyChoices, SLEIGHT_OF_HAND)) {
        printf("%d: SLEIGHT OF HAND\n", SLEIGHT_OF_HAND);
    }
    if (getProficiency(proficiencyChoices, STEALTH)) {
        printf("%d: STEALTH\n", STEALTH);
    }
    if (getProficiency(proficiencyChoices, SURVIVAL)) {
        printf("%d: SURVIVAL\n", SURVIVAL);
    }
}

/*
//Author: CJ Dvorak
//FunctionName: getProficiency
//Description: Given proficiencyBits and a throw or skill, returns 1 or 2 bits representing the proficiency
//Parameters: unsigned long long proficiencyBits: the bits for the proficiencies int throwOrSkill: the proficiency to return
//Return: 1-2 bits in int
*/
int getProficiency(unsigned long long proficiencyBits, int throwOrSkill) {
    proficiencyBits>>=throwOrSkill;
    if (throwOrSkill < 6) //Throw, so only 1 bit
        return proficiencyBits & ((unsigned long long) 1);
    else //Skills are two bits
        return proficiencyBits & ((unsigned long long) 3);
}

/*
//Author: CJ Dvorak
//FunctionName: setProficiency
//Description: Given a pointer to a proficiencyBits, a throw or skill, and a bit or two, set the proficiency in that throw or skill to the bits provided
//Parameters: unsigned long long *proficiencyBits: the value to be modified int throwOrSkill: the proficiency to be modified unsigned  long long proficiency: one or two bits to be inserted
//Return: NONE
*/
void setProficiency(unsigned long long *proficiencyBits, int throwOrSkill, unsigned long long proficiency) {
    if (throwOrSkill < 6) {
        *proficiencyBits&=~(((unsigned long long) 1) << throwOrSkill);
        *proficiencyBits|=((proficiency & 1) << throwOrSkill);
    }
    else {
        *proficiencyBits&=~(((unsigned long long) 3) << throwOrSkill);
        *proficiencyBits|=((proficiency & 3) << throwOrSkill);
    }
}

/*
//Author: CJ Dvorak
//FunctionName: hitDiceType
//Description: Return the type of hit dice that a class has
//Parameters: int class: Which class to get hit dice for
//Return: Int: number of sides on the hit dice
*/
int hitDiceType(int class) {
    switch (class) {
        case BARBARIAN:
            return 12;
        case FIGHTER:
        case PALADIN:
        case RANGER:
            return 10;
        case BARD:
        case CLERIC:
        case DRUID:
        case MONK:
        case ROGUE:
        case WARLOCK:
            return 8;
        case SORCERER:
        case WIZARD:
        default:
            return 6;
    }
}

/*
//Author: CJ Dvorak
//FunctionName: pageNumber
//Description: Return the page number in the Player Handbook that a class is on
//Parameters: int class: the class to get the page number for
//Return: int: Page number in the Player Handbook
*/
int pageNumber(int class) {
    switch (class) {
        case BARBARIAN:
            return 46;
        case BARD:
            return 51;
        case CLERIC:
            return 56;
        case DRUID:
            return 64;
        case FIGHTER:
            return 70;
        case MONK:
            return 76;
        case PALADIN:
            return 82;
        case RANGER:
            return 89;
        case ROGUE:
            return 94;
        case SORCERER:
            return 99;
        case WARLOCK:
            return 105;
        case WIZARD:
        default:
            return 112;
    }
}

////////////////////
// RACE FUNCTIONS //
////////////////////

/*
//Author: Reave Hosman
//FunctionName: addASIs
//Description: Recieves player object and adds modifiers to stats based on the characters choosen race
//Parameters: *playerCharacter: pointer to the character that you wish to modify
//Return: NONE
*/
void addASIs(struct playerCharacter *p) {
    int race = p->basicInfoArray[3];
    switch (race) {
        case HILL_DWARF:
            p->statArray[2] = p->statArray[2] + 2;
            p->statArray[4] = p->statArray[4] + 1;
            break;
        case MOUNTAIN_DWARF:
            p->statArray[2] = p->statArray[2] + 2;
            p->statArray[0] = p->statArray[0] + 2;
            break;
        case WOOD_ELF:
            p->statArray[1] = p->statArray[1] + 2;
            p->statArray[4] = p->statArray[4] + 1;
            break;
        case HIGH_ELF:
            p->statArray[1] = p->statArray[1] + 2;
            p->statArray[3] = p->statArray[3] + 1;
            break;
        case DROW:
            p->statArray[1] = p->statArray[1] + 2;
            p->statArray[5] = p->statArray[5] + 1;
            break;
        case FOREST_GNOME:
            p->statArray[3] = p->statArray[3] + 2;
            p->statArray[1] = p->statArray[1] + 1;
            break;
        case ROCK_GNOME:
            p->statArray[3] = p->statArray[3] + 2;
            p->statArray[2] = p->statArray[2] + 1;
            break;
        case STOUT_HALFLING:
            p->statArray[1] = p->statArray[1] + 2;
            p->statArray[2] = p->statArray[2] + 1;
            break;
        case LIGHTFOOT_HALFLING:
            p->statArray[1] = p->statArray[1] + 2;
            p->statArray[5] = p->statArray[5] + 1;
            break;
        case DRAGONBORN:
            p->statArray[0] = p->statArray[0] + 2;
            p->statArray[5] = p->statArray[5] + 1;
            break;
        case HALF_ELF:
            p->statArray[5] = p->statArray[5] + 2;
            break;
        case HALF_ORC:
            p->statArray[0] = p->statArray[0] + 2;
            p->statArray[2] = p->statArray[2] + 1;
    }
}



////////////////////////
/// HELPER FUNCTIONS ///
////////////////////////

/*
//Author: CJ Dvorak
//FunctionName: dice
//Description: Rolls a dice and returns result
//Parameters: int sides: Number of sides on the dice
//Return: int: Roll result
*/
int dice(int sides) {
    int roll = rand() % sides;
    if (!roll) roll = sides;
    return roll;
}

/*
//Author: Ron Gillman
//FunctionName: printStatArray
//Description: Prints out players basic Stats
//Parameters: player: Current players Character
//Return: N/A
 */
void printStatArray(int statArray[])
{
    printf("Stats:\n");
    printf("--------------------\n");
    printf("Strength: %d\n",statArray[STRENGTH]);
    printf("Dexterity: %d\n",statArray[DEXTERITY]);
    printf("Constitution: %d\n",statArray[CONSTITUTION]);
    printf("Intelligence: %d\n",statArray[INTELLIGENCE]);
    printf("Wisdom: %d\n",statArray[WISDOM]);
    printf("Charisma: %d\n",statArray[CHARISMA]);
}

/*
//Author: Ron Gillman
//FunctionName: diceRollAdder
//Description: Removes smallest dice roll and adds them all together for basic stats
//Parameters: N/A
//Return: int
 */
int diceRollAdder()
{
    int rollArray[4]; //Array of 4 dice rolls
    int lowest = 0; // Value of lowest dice roll
    int diceSum = 0; //total of three dice rolls

    for (int i = 0; i < 4; i++)
    {
        rollArray[i] = dice(6);
    }
    for (int i = 1; i < 4; i++)
    {
        if(rollArray[lowest] > rollArray[i])
        {
            lowest = i;
        }
    }
    rollArray[lowest] = 0;

    for (int i = 0; i < 4; i++)
    {
        diceSum += rollArray[i];
    }

    return diceSum;
}

/*
//Author: Ron Gillman
//FunctionName: printPlayerInfo
//Description: Asks user which character they want info from and then prints their info out.
//Parameters: currentPlayer: pointer to dynamic array of character, playerAmount: amount of players that exist.
//Return: N/A
 */
void printPlayerInfo(struct playerCharacter *currentPlayer, int playerAmount)
{
    struct playerCharacter *ptr;
    ptr = currentPlayer;
    int charChoice = 0; //Character player want to get info about

    printf("Which character do you want to see info from?\n");
    for(int i = 0; i < playerAmount; i++)
    {
        printf("[%d] %s\n",i,ptr->characterName);
        ptr++;
    }
    scanf("%d",&charChoice);


    printf("%s's Info\n",currentPlayer[charChoice].characterName);
    printf("--------------------\n");
    printBasicStats(currentPlayer[charChoice].basicInfoArray);
    printf("--------------------\n");
    printStatArray(currentPlayer[charChoice].statArray);
    printf("--------------------\n");
    printf("Profiencies:\n");
    printf("--------------------\n");
    chooseProficienciesPrintOptions(currentPlayer[charChoice].proficiencyBits);
}

/*
//Author: Ron Gillman
//FunctionName: printBasicStats
//Description: Prints out Basic Stats of character.
//Parameters:
//Return: N/A
 */
void printBasicStats(int basicStats[])
{
    printf("Basic Info:\n");
    printf("--------------------\n");
    getRace(basicStats[3]);
    getClass(basicStats[0]);
    printf("Current Level: %d\n",basicStats[1]);
    printf("Current Exp: %d\n",basicStats[2]);
    printf("Current HP: %d\n",basicStats[6]);
    printf("Maximum HP: %d\n",basicStats[5]);
    printf("Hit Dice: %d\n",basicStats[7]);
    printf("Hit Dice Type: %d\n", hitDiceType(basicStats[0]));
}

/*
//Author: Ron Gillman
//FunctionName: getClass
//Description: Prints out class of character
//Parameters: class: integer value of a class
//Return: N/A
 */
void getClass(int class)
{
    switch (class) {
        case BARBARIAN:
            printf("Class: Barbarian\n");
            break;
        case BARD:
            printf("Class: Bard\n");
            break;
        case CLERIC:
            printf("Class: Cleric\n");
            break;
        case DRUID:
            printf("Class: Druid\n");
            break;
        case FIGHTER:
            printf("Class: Fighter\n");
            break;
        case MONK:
            printf("Class: Monk\n");
            break;
        case PALADIN:
            printf("Class: Paladin\n");
            break;
        case RANGER:
            printf("Class: Ranger\n");
            break;
        case ROGUE:
            printf("Class: Rogue\n");
            break;
        case SORCERER:
            printf("Class: Sorcerer\n");
            break;
        case WARLOCK:
            printf("Class: Warlock\n");
            break;
        case WIZARD:
            printf("Class: Wizard\n");
            break;
        case HUMAN:
            printf("Class: Human\n");
            break;
    }
}

/*
//Author: Ron Gillman
//FunctionName: getRace
//Description: Prints out Race of character
//Parameters: race: integer value of a race
//Return: N/A
 */
void getRace(int race)
{
    switch (race)
    {
        case HILL_DWARF:
            printf("Race: Hill Dwarf\n");
            break;
        case MOUNTAIN_DWARF:
            printf("Race: Mountain Dwarf\n");
            break;
        case WOOD_ELF:
            printf("Race: Wood Elf\n");
            break;
        case HIGH_ELF:
            printf("Race: High Elf\n");
            break;
        case DROW:
            printf("Race: Drow\n");
            break;
        case FOREST_GNOME:
            printf("Race: Forest Gnome\n");
            break;
        case ROCK_GNOME:
            printf("Race: Rock Gnome\n");
            break;
        case STOUT_HALFLING:
            printf("Race: Stout Halfling\n");
            break;
        case LIGHTFOOT_HALFLING:
            printf("Race: Lightfoot Halfling\n");
            break;
        case DRAGONBORN:
            printf("Race: DragonBorn\n");
            break;
        case HALF_ELF:
            printf("Race: Half Elf\n");
            break;
        case HALF_ORC:
            printf("Race: Half Orc\n");
    }
}

//////////////////////
// SYSTEM FUNCTIONS //
//////////////////////

/*
//Author: Ron Gillman
//FunctionName: sourceInfo
//Description: Asks user what Browser they use, then opens them to webpage
//Parameters: N/A
//Return: N/A
 */
void sourceInfo()
{
    char userAns[2];
    userAns[1] = '\0';

    printf("First! What Web Browser do you use?\n");
    printf("[C]hrome, [F]irefox\n");

    scanf("%s",userAns);

    if(userAns[0] == 'f' || userAns[0] == 'F')
    {
        system("firefox https://gitlab.com/safetypanda/c-final-project");
    }
    else
    {
        system("chrome https://gitlab.com/safetypanda/c-final-project");
    }
}

/*
//Author: Ron Gillman
//FunctionName: editInfo
//Description: Opens OS Text Editor to edit load files
//Parameters: N/A
//Return: N/A
 */
void editInfo()
{
    char answer[2];
    answer[1] = '\0'; //WHether or not OS is Windows or Linux

    char fullCommand[75];
    printf("[W]indows or [L]inux?");
    scanf("%s",answer);

    if(answer == 'l' || answer == 'L')
    {
        printf("Type out Full Command(i.e. notepad [FILE LOCATION HERE])\n");
        scanf("%s", fullCommand);
    }
    else
    {
        printf("Type out Full Command (i.e. notepad [FILE LOCATION HERE]) \n");
        scanf("%s", fullCommand);

    }

    system(fullCommand);
}


///////////////////////
// RANDOM ENCOUNTERS //
///////////////////////

/*
//Author: CJ Dvorak
//FunctionName: getEncounter
//Description: Outputs a level-appropriate encounter, chosen randomly
//Parameters: NONE
//Return: NONE
*/
void getEncounter() {
    int input = 0;
    printf("Enter a number for player levels:\n  0: Levels 1-5\n  1: Levels 6-10\n  2: Levels 11-15\n  3: Levels 16-20\n>");
    scanf("%d", &input);
    printf("You encounter \"");
    switch(input) {
        case 0:
            switch (dice(100)) {
                case 1:
                    printf("1 hobgoblin captain with 1d4 + 1 hobgoblins");
                    break;
                case 2:
                    printf("1 chimera");
                    break;
                case 3:
                    printf("1 gorgon");
                    break;
                case 4:
                    printf("1d2 couatls");
                    break;
                case 5:
                    printf("1 ankylosaurus");
                    break;
                case 6:
                    printf("1 weretiger");
                    break;
                case 7:
                    printf("1d3 allosauruses");
                    break;
                case 8:
                case 9:
                    printf("1d3 elephants");
                    break;
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                    printf("A circle of standing stones within which the air is utterly still, no matter how hard the wind blows outside");
                    break;
                case 15:
                case 16:
                    printf("1 phase spider");
                    break;
                case 17:
                case 18:
                    printf("1 gnoll pack lord with 1d4 giant hyenas");
                    break;
                case 19:
                case 20:
                    printf("1 orog or 1 pegasus");
                    break;
                case 21:
                case 22:
                    printf("1 ankheg");
                    break;
                case 23:
                case 24:
                    printf("1d3 rhinoceroses ");
                    break;
                case 25:
                case 26:
                case 27:
                case 28:
                    printf("1d3 cockatrices");
                    break;
                case 29:
                case 30:
                case 31:
                case 32:
                    printf("1d6 + 2 giant wasps or 1d4 + 3 swarms of insects");
                    break;
                case 33:
                case 34:
                case 35:
                case 36:
                    printf("1d4 jackalweres or 1d4 scouts");
                    break;
                case 37:
                case 38:
                case 39:
                case 40:
                    printf("1d8 giant goats or 1d8 worgs");
                    break;
                case 41:
                case 42:
                case 43:
                case 44:
                    printf("2d4 hobgoblins, 2d4 orcs, or 2d4 gnolls");
                    break;
                case 45:
                case 46:
                    printf("1d2 giant poisonous snakes");
                    break;
                case 47:
                case 48:
                    printf("1d6 + 2 elk or 1d6 + 2 riding horses");
                    break;
                case 49:
                case 50:
                    printf("2d4 goblins");
                    break;
                case 51:
                case 52:
                    printf("1d3 boars");
                    break;
                case 53:
                case 54:
                    printf("1 panther (leopard) or 1 lion");
                    break;
                case 55:
                case 56:
                case 57:
                case 58:
                    printf("1d6 + 3 goblins riding wolves");
                    break;
                case 59:
                case 60:
                case 61:
                case 62:
                    printf("2d6 giant wolf spiders or 1 giant eagle");
                    break;
                case 63:
                case 64:
                case 65:
                    printf("1d8 + 4 pteranodons");
                    break;
                case 66:
                case 67:
                case 68:
                case 69:
                    printf("3d6 wolves");
                    break;
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                    printf("2d4 + 2 axe beaks");
                    break;
                case 75:
                case 76:
                    printf("1 giant boar or 1d2 tigers");
                    break;
                case 77:
                case 78:
                    printf("1 ogre or 1d3 bugbears");
                    break;
                case 79:
                case 80:
                    printf("1 giant elk, or 1 gnoll pack lord with 1d3 giant hyenas");
                    break;
                case 81:
                case 82:
                    printf("1d3 giant vultures or 1d3 hippogriffs");
                    break;
                case 83:
                case 84:
                    printf("1 goblin boss with 1d6 + 2 goblins and 1d4 + 3 wolves, or 1d3 thri-kreen");
                    break;
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                    printf("1d3 druids patrolling the wilds");
                    break;
                case 90:
                case 91:
                    printf("1d6 scarecrows or 1 wereboar");
                    break;
                case 92:
                case 93:
                    printf("1d3 centaurs or 1d3 griffons");
                    break;
                case 94:
                    printf("1d3 gnoll fangs of Yeenoghu, or 1 orc Eye of Gruumsh with 2d4 + 1 orcs");
                    break;
                case 95:
                case 96:
                    printf("1 triceratops");
                    break;
                case 97:
                    printf("1 cyclops or 1 bulette");
                    break;
                case 98:
                case 99:
                    printf("1d4 manticores");
                    break;
                case 100:
                    printf("1 tyrannosaurus rex");
                    break;
            }
            break;
        case 1:
            switch (dice(100)) {
                case 1:
                    printf("1d3 gorgons");
                    break;
                case 2:
                    printf("1d4 cyclopes");
                    break;
                case 3:
                case 4:
                    printf("1d3 gnoll fangs of Yeenoghu");
                    break;
                case 5:
                case 6:
                    printf("1 chimera ");
                    break;
                case 7:
                case 8:
                case 9:
                    printf("1d4 + 1 veterans on riding horses");
                    break;
                case 10:
                case 11:
                    printf("A tornado that touches down 1d6 miles away, tearing up the land for 1 mile before it dissipates");
                    break;
                case 12:
                case 13:
                    printf("1d3 manticores");
                    break;
                case 14:
                case 15:
                    printf("2d4 ankhegs");
                    break;
                case 16:
                case 17:
                    printf("1d8 + 1 centaurs");
                    break;
                case 18:
                case 19:
                    printf("1d6 + 2 griffons");
                    break;
                case 20:
                case 21:
                    printf("1d6 elephants");
                    break;
                case 22:
                case 23:
                case 24:
                    printf("A stretch of land littered with rotting war machines, bones, and banners of forgotten armies");
                    break;
                case 25:
                case 26:
                case 27:
                case 28:
                    printf("1d8 + 1 bugbears");
                    break;
                case 29:
                case 30:
                case 31:
                case 32:
                    printf("1 gnoll pack lord with 1d4 + 1 giant hyenas");
                    break;
                case 33:
                case 34:
                case 35:
                case 36:
                    printf("2d4 scarecrows");
                    break;
                case 37:
                case 38:
                case 39:
                case 40:
                    printf("1d12 lions");
                    break;
                case 41:
                case 42:
                case 43:
                case 44:
                    printf("1d10 thri-kreen");
                    break;
                case 45:
                case 46:
                    printf("1 allosaurus");
                    break;
                case 47:
                case 48:
                    printf("1 tiger");
                    break;
                case 49:
                case 50:
                    printf("1d2 giant eagles or 1d2 giant vultures");
                    break;
                case 51:
                case 52:
                    printf("1 goblin boss with 2d4 goblins");
                    break;
                case 53:
                case 54:
                    printf("1d2 pegasi");
                    break;
                case 55:
                case 56:
                case 57:
                case 58:
                    printf("1 ankylosaurus");
                    break;
                case 59:
                case 60:
                case 61:
                case 62:
                    printf("1d2 couatls");
                    break;
                case 63:
                case 64:
                case 65:
                case 66:
                    printf("1 orc Eye of Gruumsh with 1d8 + 1 orcs");
                    break;
                case 67:
                case 68:
                case 69:
                case 70:
                    printf("2d4 hippogriffs");
                    break;
                case 71:
                case 72:
                case 73:
                case 74:
                    printf("1d4 + 1 rhinoceroses");
                    break;
                case 75:
                case 76:
                    printf("1 hobgoblin captain with 2d6 hobgoblins");
                    break;
                case 77:
                case 78:
                    printf("1d3 phase spiders");
                    break;
                case 79:
                case 80:
                    printf("1d6 + 2 giant boars");
                    break;
                case 81:
                case 82:
                    printf("2d4 giant elk");
                    break;
                case 83:
                case 84:
                    printf("1d4 ogres and 1d4 orogs");
                    break;
                case 85:
                case 86:
                case 87:
                    printf("A hot wind that carries the stench of rot");
                    break;
                case 88:
                case 89:
                case 90:
                    printf("1d3 weretigers");
                    break;
                case 91:
                case 92:
                    printf("1 bulette");
                    break;
                case 93:
                case 94:
                    printf("A tribe of 2d20 + 20 nomads (tribal warriors) on riding horses following a herd of antelope (deer). The nomads are willing to trade food, leather, and information for weapons. ");
                    break;
                case 95:
                case 96:
                    printf("1d6 + 2 wereboars");
                    break;
                case 97:
                    printf("1 young gold dragon");
                    break;
                case 98:
                case 99:
                    printf("1d4 triceratops");
                    break;
                case 100:
                    printf("1d3 tyrannosaurus rexes");
                    break;
            }
            break;
        case 2:
            switch (dice(100)) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    printf("3d6 wereboars");
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    printf("2d10 gnoll fangs of Yeenoghu");
                    break;
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    printf("1d4 bulettes");
                    break;
                case 16:
                case 17:
                    printf("An old road of paved stones, partly reclaimed by wilderness, that travels for 1d8 miles in either direction before ending");
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                    printf("1d12 couatls");
                    break;
                case 28:
                case 29:
                case 30:
                    printf("A witch (mage) dwelling in a crude hut. She offers potions of healing, antitoxins, and other consumable items for sale in exchange for food and news.");
                    break;
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                    printf("2d10 elephants");
                    break;
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                    printf("2d4 weretigers");
                    break;
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                    printf("1d8 + 1 cyclopes");
                    break;
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                    printf("1d3 chimeras");
                    break;
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                    printf("5 triceratops");
                    break;
                case 67:
                case 68:
                case 69:
                    printf("A giant hole 50 feet across that descends nearly 500 feet before opening into an empty cave");
                    break;
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                    printf("1d4 + 3 gorgons");
                    break;
                case 80:
                case 81:
                case 82:
                case 83:
                case 84:
                case 85:
                case 86:
                case 87:
                case 88:
                    printf("1d3 young gold dragons");
                    break;
                case 89:
                case 90:
                    printf("A circular section of grass nearly a quarter-mile across that appears to have been pressed down; 1d4 more such circles connected by lines can be seen from overhead.");
                    break;
                case 91:
                case 92:
                case 93:
                case 94:
                case 95:
                case 96:
                    printf("2d4 tyrannosaurus rexes");
                    break;
                case 97:
                case 98:
                case 99:
                    printf("1 adult gold dragon");
                    break;
                case 100:
                    printf("1 ancient gold dragon");
                    break;
            }
            break;
        case 3:
            switch (dice(100)) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    printf("2d6 triceratops");
                    break;
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                    printf("1d10 gorgons");
                    break;
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                    printf("2d6 hyenas feeding on the carcass of a dead dinosaur");
                    break;
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                    printf("3d6 bulettes");
                    break;
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                    printf("A fiery chariot that races across the sky");
                    break;
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                    printf("1d3 young gold dragons");
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                    printf("2d4 cyclopes");
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                    printf("A valley where all the grass has died and the ground is littered with stumps and fallen tree trunks, all petrified");
                    break;
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                    printf("2d10 bugbears with 4d6 goblins and 2d10 wolves");
                    break;
                case 76:
                case 77:
                case 78:
                case 79:
                case 80:
                    printf("A friendly adventuring party of 1d6 + 1 characters of varying races, classes, and levels (average level 1d6 + 2). They share information about their recent travels.");
                    break;
                case 81:
                case 82:
                case 83:
                case 84:
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                    printf("1d12 chimeras");
                    break;
                case 91:
                case 92:
                case 93:
                case 94:
                case 95:
                case 96:
                    printf("1d6 + 2 tyrannosaurus rexes");
                    break;
                case 97:
                case 98:
                case 99:
                    printf("1 adult gold dragon");
                    break;
                case 100:
                    printf("1 ancient gold dragon");
                    break;
            }
            break;
    }
    printf("\"\n");
}

////////////////////////
// CHARACTER CREATION //
////////////////////////

/*
//Author: Reave Hosman
//Modified by Ron
//FunctionName: createPlayerCharacter
//Description: Constructor method for the struct playerCharacter. Takes user input and calls the proper functions to fill out character stats
//Parameters: NONE
//Return: Struct playerCharacter
*/
struct playerCharacter createPlayerCharacter() {
    //creates character from struct
    struct playerCharacter p;

    //Rolls players stat array//
    printf("Assign Stats:\n");
    rollCharacterStats(p.statArray);

    //Gives PC race and grants ASIs//
    printf("Lets start by picking your characters race. \n Please enter a: \n\t0 for a Hill Dwarf\n\t1 for a Mountain Dwarf\n\t2 for a Wood Elf\n\t3 for a High Elf\n\t4 for a Drow\n\t5 for a Forest Gnome\n\t6 for a Rock Gnome\n\t7 for a Stout Halfling\n\t8 for a Lightfoot Halfling\n\t9 for a Dragonborn\n\t10 for a Half Elf\n\t11 for a Half Orc\n\t12 for a Tiefling\n\t13 for a Human\n");
    scanf("%d",&p.basicInfoArray[2]);
    addASIs(&p);

    //Picks PC a Class//
    printf("Now lets pick your characters class.\n Enter a:\n\t0 for Barbarian\n\t1 for a Bard\n\t2 for a Cleric\n\t3 for a Druid\n\t4 for a Fighter\n\t5 for a Monk\n\t6for a paladin\n\t7 for a Ranger\n\t8 for a Rouge\n\t9 for a Sorcerer\n\t10 for a Warlock\n\t11 for a Wizard\n");
    scanf("%d", &p.basicInfoArray[0]);
    chooseProficiencies(&p);

    /////////Starting currency/////////
    //unsure if we are including this//
    startingGP(&p);

    //////Player scores///////
    //are these saving throws?

    //Starting Level of character is 1
    p.basicInfoArray[1] = 1;

    //Starting xp is 0
    p.basicInfoArray[4] = 0;

    //Set Starting HP of 10
    p.basicInfoArray[6] = 10;
    p.basicInfoArray[5] = 10;

    //Gives PC name
    printf("Enter a Name for your character [1 word, 19 characters]:\n");
    scanf("%s", p.characterName);

    return p;
}

/*
//Author: Reave Hosman
//FunctionName: startingGP
//Description: Recieves player object and calculates the characters starting gold based on their class.
//Parameters: *playerCharacter: pointer to the character that you wish to modify
//Return: NONE
//Notes: Function is called in createPlayerCharacter() however stats are never displayed. Group decided it was out of the scope of our original plan
*/
void startingGP(struct playerCharacter *p) {
    switch (p->basicInfoArray[0]) {
        case BARBARIAN:
            p->currency[3] = (dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case BARD:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case CLERIC:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case DRUID:
            p->currency[3] = (dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case FIGHTER:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case MONK:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4) + dice(4));
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case PALADIN:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case RANGER:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case ROGUE:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case SORCERER:
            p->currency[3] = (dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case WARLOCK:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
        case WIZARD:
            p->currency[3] = (dice(4) + dice(4) + dice(4) + dice(4)) * 10;
            p->currency[0] = 0;
            p->currency[1] = 0;
            p->currency[2] = 0;
            p->currency[4] = 0;
            break;
    }
}
//Ron Problems: Save and Load is only reading in one character. Some system() functions do not work in Windows, some character commands only work in Linux, had to change for Windows, now it doesn't work on Linux/Mac...