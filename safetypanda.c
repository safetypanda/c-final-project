//
// Created by safetypanda on 12/5/18.
//
/////////////////////////////////
// TEXT EDITOR AND OS DETECTOR // TO BE ADDED
/////////////////////////////////
#define TEXTEDITOR "kate /home/safetypanda/Documents/DnDChar.txt"



#include <stdio.h>
#include <string.h> //needed for name
#include <stdlib.h>





#define LINUX "/home/safetypanda/Documents/DnDChar.txt"



#include "CJ.c" //TO BE REMOVED NEED FOR FUNCTION AUTO COMPLETE

////////////////
// PROTOTYPES //
////////////////

int assignStatValue(int *diceSum);
void rollCharacterStats(int statArray[]);
void createCharacter(struct playerCharacter *currentPlayer, int playerAmount);

//HELPER PROTOTYPES
void printStatArray(int statArray[]);
int diceRollAdder();

//SAVE AND LOAD PROTOS
void load(struct playerCharacter *currentPlayer, int playerAmount);
void save(struct playerCharacter *currentPlayer, int playerAmount);

//SYSTEM PROTOS
void editInfo();
void sourceInfo();

int main()
{
    struct playerCharacter *p;
    int playerAmount;

    sourceInfo();

    printf("HOW MANY PLAYERS ARE PLAYING OR BEING LOADED?\n");
    scanf("%d",&playerAmount);

    p = (struct playerCharacter*) malloc(playerAmount * sizeof(struct playerCharacter));


    load(p,playerAmount);
    save(p,1);

    free(p);
    return 0;
}


/*
//Author: Ron Gillman
//FunctionName: createCharacter
//Description: Goes through the process of creating a character
//Parameters: *charList: pointer to current player. playerAmount: amount of players that are playing
//Return: N/A
*/
void createCharacter(struct playerCharacter *currentPlayer, int playerAmount)
{

    char name[20];
    for (int i = 0; i < playerAmount; i++)
    {
        printf("Choose A Name for your Character!\n");
        scanf("%s",name);
        strcpy(currentPlayer->characterName, name);

        rollCharacterStats(currentPlayer->statArray);

        printf("%s's Basic Stats!\n\n",currentPlayer->characterName);
        printStatArray(currentPlayer->statArray);

    }

}

/*
//Author: Ron Gillman
//FunctionName: rollCharacterStats
//Description: Lets players choose their stat value after getting the sum of 3 d6 dice rolls
//Parameters: *player: pointer to current player.
//Return: N/A
 */
void rollCharacterStats(int statArray[])
{
    int diceSum = diceRollAdder();

    for (int k = 0; k < 6 && diceSum != 0; k++)
    {
        printf("You Have %d Points Left! Assign Points for ",diceSum);

        if (k == STRENGTH)
        {
            printf("Strength!\n");
            statArray[STRENGTH] = assignStatValue(&diceSum);
        }
        if (k == DEXTERITY)
        {
            printf("Dexterity!\n");
            statArray[DEXTERITY] = assignStatValue(&diceSum);
        }
        if (k == CONSTITUTION)
        {
            printf("Constitution!\n");
            statArray[CONSTITUTION] = assignStatValue(&diceSum);
        }
        if (k == INTELLIGENCE)
        {
            printf("Intelligence!\n");
            statArray[INTELLIGENCE] = assignStatValue(&diceSum);
        }
        if (k == WISDOM)
        {
            printf("Wisdom!\n");
            statArray[WISDOM] = assignStatValue(&diceSum);
        }
        if (k == CHARISMA)
        {
            printf("Charisma!\n");
            statArray[CHARISMA] = assignStatValue(&diceSum);
        }
    }
}

/*
//Author: Ron Gillman
//FunctionName: assignStatValue
//Description: Lets player choose their stat value
//Parameters: Pass By Reference diceSum: Sum of 3 dice rolls.
//Return: int
 */
int assignStatValue(int *diceSum)
{
    int statValue = 0;
    do
    {
        scanf("%d", &statValue);
        if(statValue > *diceSum)
        {
            printf("Sorry you do not have enough points to assign that value!\n Current points:%d\n",*diceSum);
        }
    }while(statValue > *diceSum);

    *diceSum -= statValue;
    return statValue;
}

/////////////////////////////
// SAVE AND LOAD FUNCTIONS //
/////////////////////////////

/*
//Author: Ron Gillman
//FunctionName: save
//Description: Saves all characters info into a txt file.
//Parameters: *currentPlater: pointer to current player. playerAmount: amount of players that are playing
//Return: N/A
*/
void save(struct playerCharacter *currentPlayer, int playerAmount)
{
    FILE *fp;

    char saveLocation[50];
    printf("Enter your desired save location!");
    scanf("%s",saveLocation);

    fp = fopen(saveLocation, "w+");
    int tempData;
    unsigned long long int tempProf;

    for(int i = 0; i < playerAmount; i++)
    {
        fputs(currentPlayer->characterName, fp);
        for (int j = 0; j < 8; j++)
        {
            tempData = currentPlayer->basicInfoArray[j];
            putw(tempData, fp);
        }
        for (int j = 0; j < 5; j++)
        {
            tempData = currentPlayer->currency[j];
            putw(tempData, fp);
        }
        tempProf = currentPlayer->proficiencyBits;
        putw(tempProf, fp);
        for (int j = 0; j < 6; j++)
        {
            tempData = currentPlayer->scores[j];
            putw(tempData, fp);
        }
        for (int j = 0; j < 6; j++)
        {
            tempData = currentPlayer->statArray[j];
            putw(tempData, fp);
        }
        tempData = currentPlayer->tempHP;
        putw(tempData, fp);
        currentPlayer++;
    }

    printf("SAVE FILE CREATED LOCATION:");
    printf(saveLocation);
    //printf(WINDOWS); //FIGURE OUT A WAY TO DEAL WITH THIS.. MAYBE A WHERE DO YOU WANT TO SAVE?
    //printf(CUSTOM);
    printf("\n");

    fclose(fp);
}

/*
//Author: Ron Gillman
//FunctionName: load
//Description: Loads all characters back into game after being given a load location
//Parameters: *currentPlayer: pointer to current player. playerAmount: amount of players that are playing
//Return: N/A
*/
void load(struct playerCharacter *currentPlayer,int playerAmount)
{
    char tempName[20];
    int tempData;
    unsigned long long int tempProf;
    struct playerCharacter *ptr;


    char loadLocation[50];
    printf("Enter your desired load location!");
    scanf("%s",loadLocation);

    FILE *fp;
    fp = fopen(loadLocation, "r");

    ptr = currentPlayer;


    for(int i = 0; i < playerAmount; i++)
    {
        fgets(tempName, 20, fp);
        strcpy(ptr->characterName,tempName);

        for (int j = 0; j < 8; j++)
        {
            tempData = getw(fp);
            ptr->basicInfoArray[j] = tempData;

        }
        for (int j = 0; j < 5; j++)
        {
            tempData = getw(fp);
            ptr->currency[j] = tempData;;

        }
        tempProf = getw(fp);
        ptr->proficiencyBits = tempProf;

        for (int j = 0; j < 6; j++)
        {
            tempData = getw(fp);
            ptr->scores[j] = tempData;
        }
        for (int j = 0; j < 6; j++)
        {
            tempData = getw(fp);
            ptr->statArray[j] = tempData;
        }
        tempData = getw(fp);
        ptr->tempHP = tempData;

        ptr++;
    }
}

//////////////////////
// SYSTEM FUNCTIONS //
//////////////////////

/*
//Author: Ron Gillman
//FunctionName: sourceInfo
//Description: Asks user what Browser they use, then opens them to webpage
//Parameters: N/A
//Return: N/A
 */
void sourceInfo()
{
    char userAns;
    printf("First! What Web Browser do you use?\n");
    printf("[C]hrome, [F]irefox\n");

    scanf("%c",&userAns);

    if(userAns == 'f' || userAns == 'F')
    {
        system("firefox https://gitlab.com/safetypanda/c-final-project");
    }
    else
    {
        system("chrome https://gitlab.com/safetypanda/c-final-project");
    }
}
/*
//Author: Ron Gillman
//FunctionName: editInfo
//Description: Opens OS Text Editor to edit load files
//Parameters: N/A
//Return: N/A
 */
void editInfo()
{
    system(TEXTEDITOR);
}
////////////////////////
/// HELPER FUNCTIONS ///
////////////////////////

/*
//Author: Ron Gillman
//FunctionName: printStatArray
//Description: Prints out players basic Stats
//Parameters: player: Current players Character
//Return: N/A
 */
void printStatArray(int statArray[])
{
    printf("Strength:%d\n",statArray[STRENGTH]);
    printf("Dexterity:%d\n",statArray[DEXTERITY]);
    printf("Constitution:%d\n",statArray[CONSTITUTION]);
    printf("Intelligence:%d\n",statArray[INTELLIGENCE]);
    printf("Wisdom:%d\n",statArray[WISDOM]);
    printf("Charisma:%d\n",statArray[CHARISMA]);
}

/*
//Author: Ron Gillman
//FunctionName: diceRollAdder
//Description: Removes smallest dice roll and adds them all together for basic stats
//Parameters: N/A
//Return: int
 */
int diceRollAdder()
{
    int rollArray[4];
    int lowest = 0;
    int diceSum = 0;

    for (int i = 0; i < 4; i++)
    {
        rollArray[i] = dice(6);
    }
    for (int i = 1; i < 4; i++)
    {
        if(rollArray[lowest] > rollArray[i])
        {
            lowest = i;
        }
    }
    rollArray[lowest] = 0;

    for (int i = 0; i < 4; i++)
    {
        diceSum += rollArray[i];
    }

    return diceSum;
}






