//
// Created by Reave on 12/4/18
//



//////////////////////////
/// RACES ASK FOR ASI? ///
//////////////////////////
#define HILL_DWARF 0
#define MOUNTAIN_DWARF 1
#define WOOD_ELF 2
#define HIGH_ELF 3
#define DROW 4
#define FOREST_GNOME 5
#define ROCK_GNOME 6
#define STOUT_HALFLING 7
#define LIGHTFOOT_HALFLING 8
#define DRAGONBORN 9
#define HALF_ELF 10
#define HALF_ORC 11
#define TIEFLING 12


#include <stdio.h>
#include <string.h>



struct playerCharacter
{
	char characterName[20];
	int scores[6];
	int currency[5];
	int basicInfoArray[8]; // See above
	int statArray[6]; //Strength 0 //Dex 1 // consitution 2 // intelligence 3 //wisdom 4 // charisma 5
	unsigned long long proficiencyBits;
	int tempHP;
	//struct class classInfo; //Don't worry about this?
};

void addASIs(struct playerCharacter *p);


int main(){
	struct playerCharacter p;
	p.basicInfoArray[3] = HILL_DWARF;
	p.statArray[0] = 0;
	p.statArray[1] = 0;
	p.statArray[2] = 0;
	p.statArray[3] = 0;
	p.statArray[4] = 0;
	p.statArray[5] = 0;
	addASIs(&p);
	
	for(int i = 0; i < 6; i++){
		printf("%d\n", p.statArray[i]);
	}
	

	system("Pause");
	return 0;
}


 void addASIs(struct playerCharacter *p){
	int race = p-> basicInfoArray[3];
	switch (race){
		case HILL_DWARF:
			p->statArray[2] = p->statArray[2] + 2;
			p->statArray[4] = p->statArray[4] + 1;
			break;
		case MOUNTAIN_DWARF:
			p->statArray[2] = p->statArray[2] + 2;
			p->statArray[0] = p->statArray[0] + 2;
			break;
		case WOOD_ELF:
			p->statArray[1] = p->statArray[1] + 2;
			p->statArray[4] = p->statArray[4] + 1;
			break;
		case HIGH_ELF:
			p->statArray[1] = p->statArray[1] + 2;
			p->statArray[3] = p->statArray[3] + 1;
			break;
		case DROW:
			p->statArray[1] = p->statArray[1] + 2;
			p->statArray[5] = p->statArray[5] + 1;
			break;
		case FOREST_GNOME:
			p->statArray[3] = p->statArray[3] + 2;
			p->statArray[1] = p->statArray[1] + 1;
			break;
		case ROCK_GNOME:
			p->statArray[3] = p->statArray[3] + 2;
			p->statArray[2] = p->statArray[2] + 1;
			break;
		case STOUT_HALFLING:
			p->statArray[1] = p->statArray[1] + 2;
			p->statArray[2] = p->statArray[2] + 1;
			break;
		case LIGHTFOOT_HALFLING:
			p->statArray[1] = p->statArray[1] + 2;
			p->statArray[5] = p->statArray[5] + 1;
			break;
		case DRAGONBORN:
			p->statArray[0] = p->statArray[0] + 2;
			p->statArray[5] = p->statArray[5] + 1;
			break;
		case HALF_ELF:
			p->statArray[5] = p->statArray[5] + 2;
			break;
		case HALF_ORC:
			p->statArray[0] = p->statArray[0] + 2;
			p->statArray[2] = p->statArray[2] + 1;
		}






}