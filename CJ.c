#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "main.c"

void chooseProficiencies(struct playerCharacter *p);
void chooseProficienciesPrintOptions(unsigned long long proficiencyChoices);
int getProficiency(unsigned long long proficiencyBits, int throwOrSkill);
void setProficiency(unsigned long long *proficiencyBits, int throwOrSkill, unsigned long long proficiency);
int dice(int sides);
int hitDiceType(int class);
int pageNumber(int class);

/**/
int main() {
	struct playerCharacter p;
	p.basicInfoArray[0] = ROGUE;
	p.proficiencyBits = 0;

	chooseProficiencies(&p);

	printf("ARCANA: %d\n", getProficiency(p.proficiencyBits, ARCANA));
	printf("STEALTH: %d\n", getProficiency(p.proficiencyBits, STEALTH));

	system("pause");
	return 0;
}
/**/

void chooseProficiencies(struct playerCharacter *p) {
	unsigned long long profieciencyChoices = 0;
	int numChoices = 0;
	int in = 0;
	switch (p->basicInfoArray[0]) {
	case BARBARIAN:
		numChoices=2;
		setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, NATURE, 1);
		setProficiency(&profieciencyChoices, PERCEPTION, 1);
		setProficiency(&profieciencyChoices, SURVIVAL, 1);
		break;
	case BARD:
		numChoices = 3;
		setProficiency(&profieciencyChoices, ACROBATICS, 1);
		setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
		setProficiency(&profieciencyChoices, ARCANA, 1);
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, DECEPTION, 1);
		setProficiency(&profieciencyChoices, HISTORY, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, INVESTIGATION, 1);
		setProficiency(&profieciencyChoices, MEDICINE, 1);
		setProficiency(&profieciencyChoices, NATURE, 1);
		setProficiency(&profieciencyChoices, PERCEPTION, 1);
		setProficiency(&profieciencyChoices, PERFORMANCE, 1);
		setProficiency(&profieciencyChoices, PERSUASION, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		setProficiency(&profieciencyChoices, SLEIGHT_OF_HAND, 1);
		setProficiency(&profieciencyChoices, STEALTH, 1);
		setProficiency(&profieciencyChoices, SURVIVAL, 1);
		break;
	case CLERIC:
		numChoices = 2;
		setProficiency(&profieciencyChoices, HISTORY, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, MEDICINE, 1);
		setProficiency(&profieciencyChoices, PERSUASION, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		break;
	case DRUID:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
		setProficiency(&profieciencyChoices, ARCANA, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, MEDICINE, 1);
		setProficiency(&profieciencyChoices, NATURE, 1);
		setProficiency(&profieciencyChoices, PERCEPTION, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		setProficiency(&profieciencyChoices, SURVIVAL, 1);
		break;
	case FIGHTER:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ACROBATICS, 1);
		setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, HISTORY, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, PERCEPTION, 1);
		setProficiency(&profieciencyChoices, SURVIVAL, 1);
		break;
	case MONK:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ACROBATICS, 1);
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, HISTORY, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		setProficiency(&profieciencyChoices, STEALTH, 1);
		break;
	case PALADIN:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, MEDICINE, 1);
		setProficiency(&profieciencyChoices, PERSUASION, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		break;
	case RANGER:
		numChoices = 3;
		setProficiency(&profieciencyChoices, ANIMAL_HANDLING, 1);
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INVESTIGATION, 1);
		setProficiency(&profieciencyChoices, NATURE, 1);
		setProficiency(&profieciencyChoices, PERCEPTION, 1);
		setProficiency(&profieciencyChoices, STEALTH, 1);
		setProficiency(&profieciencyChoices, SURVIVAL, 1);
		break;
	case ROGUE:
		numChoices = 4;
		setProficiency(&profieciencyChoices, ACROBATICS, 1);
		setProficiency(&profieciencyChoices, ATHLETICS, 1);
		setProficiency(&profieciencyChoices, DECEPTION, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, INVESTIGATION, 1);
		setProficiency(&profieciencyChoices, PERCEPTION, 1);
		setProficiency(&profieciencyChoices, PERFORMANCE, 1);
		setProficiency(&profieciencyChoices, PERSUASION, 1);
		setProficiency(&profieciencyChoices, SLEIGHT_OF_HAND, 1);
		setProficiency(&profieciencyChoices, STEALTH, 1);
		break;
	case SORCERER:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ARCANA, 1);
		setProficiency(&profieciencyChoices, DECEPTION, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, PERSUASION, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		break;
	case WARLOCK:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ARCANA, 1);
		setProficiency(&profieciencyChoices, DECEPTION, 1);
		setProficiency(&profieciencyChoices, HISTORY, 1);
		setProficiency(&profieciencyChoices, INTIMIDATION, 1);
		setProficiency(&profieciencyChoices, INVESTIGATION, 1);
		setProficiency(&profieciencyChoices, NATURE, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
		break;
	case WIZARD:
	default:
		numChoices = 2;
		setProficiency(&profieciencyChoices, ARCANA, 1);
		setProficiency(&profieciencyChoices, HISTORY, 1);
		setProficiency(&profieciencyChoices, INSIGHT, 1);
		setProficiency(&profieciencyChoices, INVESTIGATION, 1);
		setProficiency(&profieciencyChoices, MEDICINE, 1);
		setProficiency(&profieciencyChoices, RELIGION, 1);
	}

	printf("You get your choice of %d proficiencies from the following list: \n", numChoices);
	chooseProficienciesPrintOptions(profieciencyChoices);
	while (numChoices > 0) {
		printf("Enter the number of your choice> ");
		scanf("%d", &in);
		if (getProficiency(profieciencyChoices, in) == 1) {
			setProficiency(&(p->proficiencyBits), in, 1);
			setProficiency(&profieciencyChoices, in, 0);
			numChoices--;
		}
		else
			printf("That's not a valid choice\n");
		if (numChoices>0)
			chooseProficienciesPrintOptions(profieciencyChoices);
	}
}

void chooseProficienciesPrintOptions(unsigned long long proficiencyChoices) {
	if (getProficiency(proficiencyChoices, ACROBATICS)) {
		printf("%d: ACROBATICS\n", ACROBATICS);
	}
	if (getProficiency(proficiencyChoices, ANIMAL_HANDLING)) {
		printf("%d: ANIMAL HANDLING\n", ANIMAL_HANDLING);
	}
	if (getProficiency(proficiencyChoices, ARCANA)) {
		printf("%d: ARCANA\n", ARCANA);
	}
	if (getProficiency(proficiencyChoices, ATHLETICS)) {
		printf("%d: ATHLETICS\n", ATHLETICS);
	}
	if (getProficiency(proficiencyChoices, DECEPTION)) {
		printf("%d: DECEPTION\n", DECEPTION);
	}
	if (getProficiency(proficiencyChoices, HISTORY)) {
		printf("%d: HISTORY\n", HISTORY);
	}
	if (getProficiency(proficiencyChoices, INSIGHT)) {
		printf("%d: INSIGHT\n", INSIGHT);
	}
	if (getProficiency(proficiencyChoices, INTIMIDATION)) {
		printf("%d: INTIMIDATION\n", INTIMIDATION);
	}
	if (getProficiency(proficiencyChoices, INVESTIGATION)) {
		printf("%d: INVESTIGATION\n", INVESTIGATION);
	}
	if (getProficiency(proficiencyChoices, MEDICINE)) {
		printf("%d: MEDICINE\n", MEDICINE);
	}
	if (getProficiency(proficiencyChoices, NATURE)) {
		printf("%d: NATURE\n", NATURE);
	}
	if (getProficiency(proficiencyChoices, PERCEPTION)) {
		printf("%d: PERCEPTION\n", PERCEPTION);
	}
	if (getProficiency(proficiencyChoices, PERFORMANCE)) {
		printf("%d: PERFORMANCE\n", PERFORMANCE);
	}
	if (getProficiency(proficiencyChoices, PERSUASION)) {
		printf("%d: PERSUASION\n", PERSUASION);
	}
	if (getProficiency(proficiencyChoices, RELIGION)) {
		printf("%d: RELIGION\n", RELIGION);
	}
	if (getProficiency(proficiencyChoices, SLEIGHT_OF_HAND)) {
		printf("%d: SLEIGHT OF HAND\n", SLEIGHT_OF_HAND);
	}
	if (getProficiency(proficiencyChoices, STEALTH)) {
		printf("%d: STEALTH\n", STEALTH);
	}
	if (getProficiency(proficiencyChoices, SURVIVAL)) {
		printf("%d: SURVIVAL\n", SURVIVAL);
	}
}

int getProficiency(unsigned long long proficiencyBits, int throwOrSkill) {
	proficiencyBits>>=throwOrSkill;
	if (throwOrSkill < 6) //Throw, so only 1 bit
		return proficiencyBits & ((unsigned long long) 1);
	else //Skills are two bits
		return proficiencyBits & ((unsigned long long) 3);
}

void setProficiency(unsigned long long *proficiencyBits, int throwOrSkill, unsigned long long proficiency) {
	if (throwOrSkill < 6) {
		*proficiencyBits&=~(((unsigned long long) 1) << throwOrSkill);
		*proficiencyBits|=((proficiency & 1) << throwOrSkill);
	}
	else {
		*proficiencyBits&=~(((unsigned long long) 3) << throwOrSkill);
		*proficiencyBits|=((proficiency & 3) << throwOrSkill);
	}
}

int dice(int sides) {
	int roll = rand() % sides;
	if (!roll) roll = sides;
	return roll;
}

int hitDiceType(int class) {
	switch (class) {
	case BARBARIAN:
		return 12;
	case FIGHTER:
	case PALADIN:
	case RANGER:
		return 10;
	case BARD:
	case CLERIC:
	case DRUID:
	case MONK:
	case ROGUE:
	case WARLOCK:
		return 8;
	case SORCERER:
	case WIZARD:
	default:
		return 6;
	}
}

int pageNumber(int class) {
	switch (class) {
	case BARBARIAN:
		return 46;
	case BARD:
		return 51;
	case CLERIC:
		return 56;
	case DRUID:
		return 64;
	case FIGHTER:
		return 70;
	case MONK:
		return 76;
	case PALADIN:
		return 82;
	case RANGER:
		return 89;
	case ROGUE:
		return 94;
	case SORCERER:
		return 99;
	case WARLOCK:
		return 105;
	case WIZARD:
	default:
		return 112;
	}
}