# C&D&D Version 1.0

Created By: James Gillman, Reave Hosman. CJ Dvorak for 200 Level C Class

What it do:

    - Saves and Loads all of your DnD Characters!
    
    - Create new Characters!
    
    - Keeps track of the hard stuff for you!
    
    - Allows for editing some character info!
    
    - Generates Random Encounters for you!
    
    - If there's a memory leak it'll keep your computer nice and toasty.
    
What it don't do:

    - Love you
    
    - Save any marriages
    
    - Give out winning lottery tickets
    
    - Look nice.
    
Issues:
    
    -Save and load only like one character, multiple is causing issues.
    
    -Not using Currency, it's made and being filled, but no way to parse that info.
    
    - In quick desperation I used getw() and setw() for File IO, this doesn't work on Windows machines.
        -Fix: Get a proper operating system. ;) This is getting fixed, top priority.
        
    - Viewing Source code only works on Linux (Haven't tested on Mac yet).
    
To Do:

    - Better UI.
    
    - Handle Dice Rolls and adding stats.
    
    - Character Leveling
    
    - Items.
    
    - AND much more.

